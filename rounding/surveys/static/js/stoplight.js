"use strict";
function set_header(){
    $("h2").html("Stop Light Report for " + $("#quarter-choice").find(":selected").text());
    var dpt = $("#department-choice").find(":selected");
    if (dpt.val()==="all"){
        $("h5").html("All Departments");
    }else{
        $("h5").html(dpt.text());
    }
}

function get_quarter(){
    var new_q = $("#quarter-choice");
    var dpt = $("#department-choice").val() || "";
    $("#stoplight-table-wrapper").html("<em>Fetching Data...</em>");
    $.getJSON(Django.url("generate-stop-light",{pk:+new_q.val()})+"?dept="+dpt,function(data){
        $("#stoplight-table-wrapper").html(data.table);
        $("#permalink").attr("href",Django.url("static-stop-light", {pk:new_q.val()})+"?dept="+dpt);
        set_header();
    });
}
$(document).ready(function(){
    $("#quarter-choice, #department-choice").select2().addClass("input-large");
    set_header();
    $("#quarter-choice, #department-choice").on("change", get_quarter);
    $("#print").click(function(){
        window.print();
        return true;
    });
    get_quarter();
});

