"use strict";
$(document).ready(function(){

    var columnFilterDefs = [
        {type: "text"},
        {type: "text"},
        {type: "select", values:QuarterDetails.reports_to},
        {type: "select", values:QuarterDetails.departments},
        null,
        null //action
    ];
    var columnDefs = [
        null,
        null, //suggested
        null, //type
        null,//status
        null, //quarter completed
        {bSortable: false}
    ];

    $('#rq-table').dataTable({
        bPaginate: true,
        sPaginationType: "bootstrap",
        bProcessing: true,
        bServerSide: true,
        iDisplayLength:100,
        bSaveState:true,
        sAjaxSource: Django.url('rounding-quarter-dt',{pk:QuarterDetails.quarter}),
        bAutoWidth:false,
        fnAdjustColumnSizing:true,
        aoColumns:columnDefs,
        bFilter:true,
        sDom: '<"row-fluid"<"span6"ir><"span6"p>>t<"row-fluid"<"span12"lp>>',
        fnServerData: function ( sSource, aoData, fnCallback ) {
            aoData.push( { "name":"quarter", "value":QuarterDetails.quarter } );
            $.getJSON( sSource, aoData, function (json) {
                fnCallback(json)
            } );
        }
    }).columnFilter({
        sPlaceHolder: "head:after",
        aoColumns: columnFilterDefs,
        iFilteringDelay:250
    }).find("select, input").addClass("input-small");


    $("select,input").addClass("input-medium");

    $("#rq-table").on('click','a.active-toggle',function(){
        var url = Django.url('toggle-active',{pk:$(this).data("staff")});
        url += "?q="+QuarterDetails.quarter;
        var self = $(this);

        $.get(url,function(data){

            var status=data.status;
            var icon = self.find('i');
            var span = self.find("span");
            icon.removeClass("icon-ok-sign icon-minus-sign");
            span.text(status);
            if (status == "Inactive"){
                icon.addClass("icon-minus-sign");
            }else{
                icon.addClass("icon-ok-sign");
            }
        });
        return false;
    });

});

