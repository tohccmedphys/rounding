"use strict";
$(document).ready(function(){

    $("body").on("click", "input.sli-select", function(){

        var checked = $(".sli-select:checked");

        if (checked.length > 1){
            $("#duplicates").val(
                checked.map(function(e){return $(this).val()}).get().join(",")
            );
            $("#duplicate-form").show();
        }else{
            $("#duplicate-form").hide();
        }

    });

    var statuses= _.map(StopLightItems.status,function(s){
        return {value:s[0], label:s[1]};
    });
    var types= _.map(StopLightItems.types,function(s){
        return {value:s[0], label:s[1]};
    });
    var quarters = _.map(StopLightItems.quarters,function(s){
        return {value:s[0], label:s[1]};
    });

    var columnFilterDefs = [
        null, //action
        {type: "select", values:quarters},
        {type: "select", values:types},
        {type: "select", values: statuses},
        {type: "select", values:quarters},
        {type: "text"},
        null,
        null //action
    ];
    var columnDefs = [
        {bSortable:false},//description
        null, //suggested
        null, //type
        null,//status
        null, //quarter completed
        {bSortable:false},//description
        null, //votes
        {bSortable:false}//actions
    ];

    $('#sli-table').dataTable({
        bSaveState:true,
        bPaginate: true,
        sPaginationType: "bootstrap",
        bProcessing: true,
        bServerSide: true,
        sAjaxSource: Django.url('stop-light-items-dt'),
        bAutoWidth:false,
        fnAdjustColumnSizing:true,
        aoColumnDefs:[
            {bSortable: false, aTargets:[-1]}
        ],
        bFilter:true,
        sDom: '<"row-fluid"<"span6"ir><"span6"p>>t<"row-fluid"<"span12"lp>>'
    }).columnFilter({
        sPlaceHolder: "head:after",
        aoColumns: columnFilterDefs,
        iFilteringDelay:250
    }).find("select, input").addClass("input-small");


    $("select,input").addClass("input-medium");
});

