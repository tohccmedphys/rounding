"use strict";
function set_header(){
    $("h2").html("Rounding Summary Report for " + $("#quarter-choice").find(":selected").text());
}
$(document).ready(function(){
    $("#quarter-choice").select2().addClass("input-large");
    set_header();

    $("#quarter-choice").on("change", function(){
        var new_q = $(this);
        $("#summary-table-wrapper").html("<em>Fetching Data...</em>");
        $.getJSON(Django.url("generate-summary",{pk:new_q.val()}),function(data){
            $("#summary-table-wrapper").html(data.table);
            set_header();
        });
    });

    $("#print").click(function(){
        window.print();
        return true;
    });
});

