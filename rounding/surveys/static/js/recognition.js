"use strict";
function set_header(){
    $("h2").html("Rounding Recognition Report for " + $("#quarter-choice").find(":selected").text());
}

function get_quarter(){
    var new_q = $("#quarter-choice");
    $("#recognition-table-wrapper").html("<em>Fetching data...</em>");
    $.getJSON(Django.url("generate-recognition",{pk:new_q.val()}),function(data){
        $("#recognition-table-wrapper").html(data.table);
        set_header();
    });
}
$(document).ready(function(){
    $("#quarter-choice").select2().addClass("input-large");
    set_header();
    $("#quarter-choice").on("change", get_quarter);
    $("#print").click(function(){
        window.print();
        return true;
    });
    get_quarter();
});

