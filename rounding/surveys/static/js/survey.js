"use strict";

$(document).ready(function(){
    var SLI_DELIM = "&STOP_LIGHT_ITEM&";
    $('#add_more').click(function() {
        var form_idx = $('#id_form-TOTAL_FORMS').val();
        $('#form_set').append($('#empty_form').html().replace(/__prefix__/g, form_idx));
        $('#id_form-TOTAL_FORMS').val(parseInt(form_idx) + 1);
        $("select:visible").addClass("input-large").not("[readonly]").select2();
    });
    $("select:visible").addClass("input-large").not("[readonly]").select2();
    $("a.add-sli").click(function(){
        var parent  = $(this).parent().siblings(".controls");
        var inputs =  parent.find("textarea");
        var txt = inputs.last();
        var inp = '<br/><textarea></textarea>';
        txt.after(inp);
    });

    $("#report-form").submit(function(evt){
       $("div.add-more-wrapper").each(function(e){
           var texts = $(this).find("textarea").map(function(i,e){return $(e).val()});
            texts = _.filter(texts, function(t){return $.trim(t).length > 0 });
            $(this).find("input[type=hidden]").val(texts.join(SLI_DELIM));
       });
    });

       $("div.add-more-wrapper").each(function(e){
            var texts = $(this).find("input[type=hidden]").val().split(SLI_DELIM);
            var input =  $(this).find("textarea");
            var inp;

            if (texts.length > 0){
                input.val(texts[0]);
                for (var i=1; i<texts.length;i++){
                    inp = '<br/><textarea>'+texts[i]+'</textarea>';
                    input.after(inp);
                }
            }
       });
});
