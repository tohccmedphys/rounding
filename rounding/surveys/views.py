from django import http
from django.conf import settings
from django.core.mail import send_mail
from django.core.urlresolvers import reverse, reverse_lazy
from django.db import transaction
from django.db.models import Count
from django.http import HttpResponseRedirect, Http404
from django.forms.models import model_to_dict
from django.views.generic import TemplateView, View, CreateView, DetailView, ListView, View, FormView, UpdateView
from django.views.generic.base import ContextMixin
from django.contrib import messages
from django.utils.translation import ugettext as _
from django.utils import formats, timezone

import django.template.loader
from django.template import Context, RequestContext
import itertools
from easytables.views import BaseDataTablesView
from easytables.views import  Column as C
from braces.views import JSONResponseMixin, PermissionRequiredMixin, AjaxResponseMixin, LoginRequiredMixin

import json
import forms
from . import models
import staff.models as staff_models

JSON_DATE_FORMAT = formats.get_format("DATE_INPUT_FORMATS")[0]


class Rounding(LoginRequiredMixin, CreateView):

    model = models.Rounding
    form_class = forms.RoundingForm

    def form_valid(self, form):
        user = self.request.user
        form.instance.rounded_by = user

        context = self.get_context_data()
        formset = context["formset"]
        if not formset.is_valid():

            messages.error(self.request, _("Please correct the errors in red below and submit again." ))
            context["form"] = form
            return self.render_to_response(context)

        super(Rounding, self).form_valid(form)

        for f in formset:
            if f.has_changed():
                f.instance.rounding = self.object
                f.save()

        models.generate_stop_light_items_for_rounding(self.object)

        messages.success(self.request, _("Successfully rounded %s " % self.object.staff.name()))
        return HttpResponseRedirect(reverse("current-quarter"))

    def get_initial(self):

        init = super(Rounding, self).get_initial()

        self.staff = None
        if "staff_pk" in self.kwargs:
            self.staff = staff_models.Staff.objects.get(pk=self.kwargs.get("staff_pk"))
            init['staff'] = self.staff.pk

        return init


    def get_context_data(self, *args, **kwargs):

        context = super(Rounding, self).get_context_data(*args, **kwargs)

        context["prev"] = []
        if self.staff:
            context["prev"] = self.staff.rounding_set.order_by("-rounded_on").filter(
                rounded_on__lte=timezone.now()
            )[:2]

        if self.request.POST:
            context["formset"] = forms.RecognitionFormset(self.request.POST)
        else:
            context["formset"] = forms.RecognitionFormset(queryset=models.Recognition.objects.none())

        context["add_more_fields"] = models.Rounding.STOP_LIGHT_ATTRS

        if self.request.POST:
            for field in models.Rounding.STOP_LIGHT_ATTRS:
                context["extra_%s"%field] = [x for x in self.request.POST.getlist(field) if x][:-1]

        return context


class RoundingDetail(UpdateView):

    model = models.Rounding
    form_class = forms.RoundingUpdateForm
    template_name_suffix = "_update_form"

    def form_valid(self, form):
        context = self.get_context_data()
        formset = context["formset"]
        if not formset.is_valid():

            messages.error(self.request, _("Please correct the errors in red below and submit again." ))
            context["form"] = form
            return self.render_to_response(context)

        super(RoundingDetail, self).form_valid(form)

        for f in formset:
            if f.has_changed():
                f.instance.rounding = self.object

                if 'thank_you_sent' in f.changed_data:
                    if f.cleaned_data['thank_you_sent']:
                        f.instance.thank_you_sent_by = self.request.user
                    else:
                        f.instance.thank_you_sent_by = None

                f.save()

        messages.success(self.request, _("Successfully updated %d " % self.object.pk))
        return HttpResponseRedirect(reverse("rounding-detail",kwargs={"pk":self.object.pk}))

    def get_context_data(self, *args, **kwargs):

        context = super(RoundingDetail, self).get_context_data(*args, **kwargs)

        if self.request.POST:
            context["formset"] = forms.UpdateRecognitionFormset(self.request.POST, instance= self.object)
        else:
            context["formset"] = forms.UpdateRecognitionFormset(instance= self.object)

        return context


class RoundingQuarterDetailData(BaseDataTablesView):

    model = staff_models.Staff

    columns = [
        C("last_name","icontains", "last_name"),
        C("first_name","icontains", "first_name"),
        C("reports_to", "pk", "reports_to__last_name"),
        C("department", "pk", "department__title"),
        C("fyqtr_status",None, "inactivequarter"),
        C("this_quarter", None, None),
    ]

    def __init__(self, *args, **kwargs):
        super(RoundingQuarterDetailData, self).__init__(*args, **kwargs)
        supervisors = staff_models.Staff.objects.filter(is_supervisor=True)
        self.supervisors = {s.pk:s.name_alt() for s in supervisors}


    def get_queryset(self):
        qs = super(RoundingQuarterDetailData, self).get_queryset()
        qs = qs.select_related("department")
        return qs

    def get(self, *args, **kwargs):
        self.quarter = models.RoundingQuarter.objects.get(pk=self.request.GET.get("quarter"))
        self.rounded_this_quarter = {r.staff.pk:r for r in models.Rounding.objects.filter(roundingquarter=self.quarter).select_related("staff__pk")}

        self.inactive_this_quarter = models.InactiveQuarter.objects.filter(quarter=self.quarter).values_list("staff__pk", flat=True)
        return super(RoundingQuarterDetailData, self).get(*args, **kwargs)

    def get_last_name_display(self, staff):

        rounding = self.rounded_this_quarter.get(staff.pk,None)

        if rounding:
            url = reverse("rounding-detail", kwargs={"pk":rounding.pk})
        else:
            url = reverse("round-staff", kwargs={"staff_pk":staff.pk})

        return '<a href="%s">%s</a>' % (url, staff.last_name)

    def get_reports_to_display(self, staff):

        try:
            return self.supervisors[staff.reports_to_id]
        except KeyError:
            return ""

    def get_department_display(self, staff):
        return staff.department.title if staff.department else "<em>N/A</em>"

    def get_fyqtr_status_display(self, staff):

        is_inactive = staff.pk in self.inactive_this_quarter
        if not is_inactive:
            icon = "icon-ok-sign"
        else:
            icon = "icon-minus-sign"

        url = reverse("toggle-active",kwargs={"pk":staff.pk})

        a = '<a class="btn btn-mini active-toggle" data-staff="%s" href="#" title="Click to toggle this users active status"><i class="%s"></i> <span class="status-text">%s</span></a>' % (staff.pk, icon, staff.status(quarter=self.quarter, is_inactive=is_inactive))

        return a

    def get_this_quarter_display(self, staff):
        rounding = self.rounded_this_quarter.get(staff.pk,None)

        if rounding:
            text = rounding.rounded_on.strftime(JSON_DATE_FORMAT)

            c = rounding.recognition_set.count()
            if c:
                text += ' (%d <i class="icon-thumbs-up"></i>)' % c
            title = "%d recognitions" % c
            url = reverse("rounding-detail", kwargs={"pk":rounding.pk})
        else:
            text = "Round Now"
            url = reverse("round-staff", kwargs={"staff_pk":staff.pk})
            title = "Click to round this person now"

        return '<a href="%s" title="%s">%s</a>' % (url, title, text)


class RoundingQuarterList(ListView):
    model = models.RoundingQuarter
    paginate_by = 20


class RoundingQuarterDetail(DetailView):
    model = models.RoundingQuarter

    def get_object(self):
        if 'pk' not in self.kwargs:
            self.kwargs["pk"] = self.model.objects.get(default=True).pk
        return super(RoundingQuarterDetail, self).get_object()

    def get_context_data(self, *args, **kwargs):

        context = super(RoundingQuarterDetail, self).get_context_data(*args, **kwargs)

        data = {
            "departments":[{"value":d.pk,"label":d.title} for d in staff_models.Department.objects.all()],
            "reports_to":[{"value":r.pk,"label":r.name()} for r in staff_models.Staff.objects.filter(is_supervisor=True)],
            "statuses":[{"value":s[0],"label":s[1]} for s in staff_models.FYQTR_ACTIVE_CHOICES],
        }
        context["dt_data"] = json.dumps(data)

        return context



class DeDuplicate(View):

    def post(self, *args, **kwargs):
        with transaction.atomic():
            sli = models.StopLightItem.objects.get(pk=self.kwargs["pk"])
            parent = sli.duplicate_of
            parent.votes -=  1
            parent.save()
            sli.votes = 1
            sli.duplicate_of = None
            sli.save()
            messages.success(self.request, _("Stop Light Item %d is no longer marked as a duplicate"% sli.pk))

        return HttpResponseRedirect(reverse("stop-light-items"))


class MarkDuplicate(View):

    def post(self, *args, **kwargs):

        with transaction.atomic():
            dups = self.request.POST.get("duplicates").split(',')
            slis = models.StopLightItem.objects.filter(pk__in=dups)
            count = slis.count()
            if count > 1:
                parent, children = slis[0], slis[1:]
                count = children.count()
                for child in children:
                    parent.votes += child.votes
                    child.duplicate_of = parent
                    child.votes = 0
                    child.save()

                parent.save()
                messages.success(self.request, _("Marked %d StopLightItems as duplicates" % (count)))

            return HttpResponseRedirect(reverse("stop-light-items"))



class StopLightItem(UpdateView):
    model = models.StopLightItem
    form_class = forms.StopLightItemForm
    success_url = reverse_lazy("stop-light-items")


class StopLightItems(ListView):

    model = models.StopLightItem

    def get_context_data(self, *args, **kwargs):

        context = super(StopLightItems, self).get_context_data(*args, **kwargs)
        quarters = [(x.pk, x.name) for x in models.RoundingQuarter.objects.all()]
        context['quarters'] = json.dumps(quarters)
        context['types'] = json.dumps(models.STOP_LIGHT_TYPES)
        context['status'] = json.dumps(models.STOP_LIGHT_STATUS_CHOICES)
        return context


class StopLightItemsData(BaseDataTablesView):

    model = models.StopLightItem

    columns = [
        C("select", None, None),
        C("rounding","roundingquarter", ("rounding__roundingquarter__year", "rounding__roundingquarter__quarter")),
        C("type", "icontains", "type"),
        C("status", "icontains", "status"),
        C("quarter_completed", "pk", "quarter_completed"),
        C("description", "icontains", None),
        C("votes", None, "votes"),
        C("actions", None, None),
    ]

    def get_queryset(self):
        qs = super(StopLightItemsData, self).get_queryset()
        qs = qs.select_related("rounding__roundingquarter", "quarter_completed")
        return qs

    def get_votes_display(self, sli):
        return "<em>Duplicate</em>"  if sli.duplicate_of else sli.votes

    def get_completed_display(self, sli):
        return sli.quarter_completed.name if sli.quarter_completed else ""

    def get_type_display(self, sli):
        return sli.type_display()

    def get_status_display(self, sli):
        return sli.status_display()

    def get_rounding_display(self, sli):
        return sli.rounding.roundingquarter.name

    def get_actions_display(self, sli):
        t = django.template.loader.get_template("surveys/_stoplightitem_actions.html")
        c = Context({"instance":sli})
        return t.render(c)

    def get_description_display(self, sli):
        t = django.template.loader.get_template("surveys/_stoplightitem_description.html")
        c = Context({"instance":sli})
        return t.render(c)

    def get_select_display(self, sli):
        t = django.template.loader.get_template("surveys/_stoplightitem_select.html")
        c = RequestContext(self.request, {"instance":sli})
        return t.render(c)


class RoundingSummary(TemplateView):

    template_name = "surveys/rounding_summary.html"


    def get_context_data(self, *args, **kwargs):

        context = super(RoundingSummary, self).get_context_data(*args, **kwargs)
        quarters = models.RoundingQuarter.objects.all()
        context["quarters"] = quarters
        template = django.template.loader.get_template("surveys/_summary_table.html")
        summary = models.RoundingQuarter.objects.default().summary()
        summary_context = Context({"summary":summary})
        context["summary_table"] = template.render(summary_context)

        return context


class GenerateSummary(JSONResponseMixin, View):

    def get(self, *args, **kwargs):
        """process file, apply calculation procedure and return results"""

        try:
            rounding_quarter = models.RoundingQuarter.objects.get(pk=self.kwargs["pk"])
        except models.RoundingQuarter.DoesNotExist:
            raise Http404

        summary = rounding_quarter.summary()
        results = {}
        template = django.template.loader.get_template("surveys/_summary_table.html")
        summary_context = Context({"summary":summary})
        results["table"] = template.render(summary_context)

        return self.render_json_response(results)


class RecognitionReport(TemplateView):

    template_name = "surveys/recognition_report.html"


    def get_context_data(self, *args, **kwargs):

        context = super(RecognitionReport, self).get_context_data(*args, **kwargs)
        quarters = models.RoundingQuarter.objects.all()
        context["quarters"] = quarters
        return context


class RecognitionStars(TemplateView):

    template_name = "surveys/recognition_stars_report.html"


    def get_context_data(self, *args, **kwargs):

        context = super(RecognitionStars, self).get_context_data(*args, **kwargs)
        quarters = models.RoundingQuarter.objects.all()
        context["quarters"] = quarters
        context["threshold"] = settings.STAR_THRESHOLD
        return context


class GenerateRecognitionStars(JSONResponseMixin, View):

    def get(self, *args, **kwargs):
        """process file, apply calculation procedure and return results"""

        try:
            rounding_quarter = models.RoundingQuarter.objects.get(pk=self.kwargs["pk"])
        except models.RoundingQuarter.DoesNotExist:
            raise Http404


        all_recs =  models.Recognition.objects.filter( rounding__roundingquarter=rounding_quarter)

        stars = all_recs.values( "recognized").annotate(
                Count("recognized")
            ).order_by(
                "recognized__count"
            ).filter(
                recognized__count__gte=settings.STAR_THRESHOLD
            ).select_related("staff")


        recognitions = all_recs.filter(
            recognized__in=[x["recognized"] for x in stars]
        ).order_by("recognized").select_related("recognized")


        results = {}
        template = django.template.loader.get_template("surveys/_recognition_stars_table.html")
        context = Context({"recognitions":recognitions, })
        results["table"] = template.render(context)

        return self.render_json_response(results)


class GenerateRecognitions(JSONResponseMixin, View):

    def get(self, *args, **kwargs):
        """process file, apply calculation procedure and return results"""

        try:
            rounding_quarter = models.RoundingQuarter.objects.get(pk=self.kwargs["pk"])
        except models.RoundingQuarter.DoesNotExist:
            raise Http404

        recognitions = models.Recognition.objects.filter(
            rounding__roundingquarter=rounding_quarter
        ).select_related(
            "recognized",
            "rounding__staff",
            "recognized__reports_to",
        ).order_by("recognized__reports_to")


        results = {}
        template = django.template.loader.get_template("surveys/_recognition_table.html")
        context = Context({"recognitions":recognitions})
        results["table"] = template.render(context)

        return self.render_json_response(results)


class RawResponseReport(TemplateView):

    template_name = "surveys/raw_responses.html"

    def get_context_data(self, *args, **kwargs):

        context = super(RawResponseReport, self).get_context_data(*args, **kwargs)
        context["departments"] = staff_models.Department.objects.all()
        context["quarters"] = models.RoundingQuarter.objects.all()
        return context

class GenerateRawResponseReport(JSONResponseMixin, View):

    def get(self, *args, **kwargs):
        """process file, apply calculation procedure and return results"""

        try:
            rounding_quarter = models.RoundingQuarter.objects.get(pk=self.kwargs["pk"])
        except models.RoundingQuarter.DoesNotExist:
            raise Http404

        roundings = models.Rounding.objects.filter(
            roundingquarter=rounding_quarter
        ).order_by(
            "staff__department__title",
            "staff__last_name"
        ).select_related(
            "staff__department",
            "rounded_by",
            "roundingquarter",
        )

        recognitions = models.Recognition.objects.filter(
            rounding__in=roundings
        ).select_related("recognized")

        for r in roundings:
            r.recognitions = [x for x in recognitions if x.rounding_id == r.pk]
            for f in models.Rounding.STOP_LIGHT_ATTRS:
                setattr(r,f,r.display_sli(f))

        template = django.template.loader.get_template("surveys/_raw_responses.html")
        results = {
            "table":template.render(Context({ "roundings":roundings, })),
        }

        return self.render_json_response(results)

#class StaticStopLightReport(TemplateView):
#
#    template_name = "surveys/stoplight_report_static.html"
#
#    def get_context_data(self, *args, **kwargs):
#
#        context = super(StaticStopLightReport, self).get_context_data(*args, **kwargs)
#
#        try:
#            rounding_quarter = models.RoundingQuarter.objects.get(pk=self.kwargs["pk"])
#        except models.RoundingQuarter.DoesNotExist:
#            raise Http404
#
#        completed = models.StopLightItem.objects.filter(
#            quarter_completed=rounding_quarter,
#            status=models.SL_COMPLETE,
#        )
#        working_on = models.StopLightItem.objects.filter(
#            status=models.SL_INPROGRESS
#        )
#
#        invalid = models.StopLightItem.objects.filter(
#            quarter_completed=rounding_quarter,
#            status=models.SL_INVALID,
#        )
#
#        rows = itertools.izip_longest(completed, working_on, invalid)
#        results = {}
#        template = django.template.loader.get_template("surveys/_stoplight_table.html")
#
#        tbl_context = Context({"rows":rows})
#        context["table"] = template.render(tbl_context)
#        context["quarter"] = rounding_quarter
#        return context
#





class StopLightReport(TemplateView):

    template_name = "surveys/stoplight_report.html"


    def get_context_data(self, *args, **kwargs):

        context = super(StopLightReport, self).get_context_data(*args, **kwargs)
        context["departments"] = staff_models.Department.objects.all()
        context["quarters"] = models.RoundingQuarter.objects.all()
        return context


class StaticStopLightReport(TemplateView):

    template_name = "surveys/stoplight_report_static.html"

    def get_context_data(self, *args, **kwargs):

        context = super(StaticStopLightReport, self).get_context_data(*args, **kwargs)

        try:
            rounding_quarter = models.RoundingQuarter.objects.get(pk=self.kwargs["pk"])
        except models.RoundingQuarter.DoesNotExist:
            raise Http404

        all_results = models.StopLightItem.objects.all()

        dpt = self.request.GET.get("dept","all").strip() or "all"
        dpt_name = "All Departments"
        if dpt != "all":
            try:
                dpt = int(dpt)
                dpt_name = staff_models.Department.objects.get(pk=dpt).title
                all_results = all_results.filter(rounding__staff__department=dpt)
            except (ValueError,staff_models.Department.DoesNotExist):
                pass


        quarter_results = all_results.filter(quarter_completed=rounding_quarter)

        completed = quarter_results.filter( status=models.SL_COMPLETE,)
        invalid = quarter_results.filter( status=models.SL_INVALID)
        working_on = all_results.filter(status=models.SL_INPROGRESS)

        rows = itertools.izip_longest(completed, working_on, invalid)
        results = {}
        template = django.template.loader.get_template("surveys/_stoplight_table.html")

        tbl_context = Context({"rows":rows})
        context["table"] = template.render(tbl_context)
        context["quarter"] = rounding_quarter
        context["department"] = dpt_name
        return context


class GenerateStopLight(JSONResponseMixin, View):

    def get(self, *args, **kwargs):
        """process file, apply calculation procedure and return results"""

        try:
            rounding_quarter = models.RoundingQuarter.objects.get(pk=self.kwargs["pk"])
        except models.RoundingQuarter.DoesNotExist:
            raise Http404

        all_results = models.StopLightItem.objects.all()

        dpt = self.request.GET.get("dept","all").strip()
        if dpt != "all":
            all_results = all_results.filter(rounding__staff__department=dpt)

        quarter_results = all_results.filter(quarter_completed=rounding_quarter)

        completed = quarter_results.filter( status=models.SL_COMPLETE,)
        invalid = quarter_results.filter( status=models.SL_INVALID)
        working_on = all_results.filter(status=models.SL_INPROGRESS)

        rows = itertools.izip_longest(completed, working_on, invalid)
        results = {}
        template = django.template.loader.get_template("surveys/_stoplight_table.html")
        context = Context({
            "rows":rows,
        })
        results["table"] = template.render(context)

        return self.render_json_response(results)


