from django.test import TestCase
from .. import models, forms
from staff import models as smodels

from django.conf import settings
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.forms import ValidationError
from django.http import Http404
from django.test.client import Client

import django.utils.timezone as timezone
from faker import Faker
import random

faker = Faker()

MAR = 3
JUN = 6
SEP = 9
DEC = 12

class TestRecognitionForm(TestCase):

    def setUp(self):

        self.password = 'password'

        self.admin = User.objects.create_superuser('admin', 'myemail@test.com', self.password)

        p = smodels.Position(title="title", slug="slug")
        p.save()
        sp = smodels.Position(title="super_title", slug="super_slug")
        sp.save()
        dpt =  smodels.Department(title="title",slug="slug")
        dpt.save()

        self.super = smodels.Staff(
            first_name="super",
            last_name="visor",
            position=sp,
            department=dpt,
            is_supervisor=False,
            profile=self.admin
        )
        self.super.save()

        self.staff = smodels.Staff(
            first_name="first",
            last_name="last",
            position=p,
            position_no="1234",
            reports_to=self.super,
        )
        self.staff.save()

    def test_duplicate_recognized(self):

        data = { "recognized":self.staff.pk, "recognized_other":"test", }
        f = forms.RecognitionForm(data=data)
        with self.assertRaises(ValidationError):
            f.is_valid()
            f.clean()

    def test_ok(self):

        data = { "recognized":self.staff.pk, "staff_type":models.Recognition.STAFF,"reason":"ok" }
        f = forms.RecognitionForm(data=data)
        self.assertTrue(f.is_valid())


class TestRoundingForm(TestCase):

    def test_writeable(self):
        f = forms.RoundingForm()
        self.assertNotIn("readonly", f.fields["roundingquarter"].widget.attrs)
        self.assertNotIn("readonly", f.fields["staff"].widget.attrs)

    def test_readonly_for_cur_quarter(self):
        f = forms.RoundingForm(initial={"staff":"1"})
        self.assertIn("readonly", f.fields["roundingquarter"].widget.attrs)
        self.assertIn("readonly", f.fields["staff"].widget.attrs)


class TestSLIForm(TestCase):

    def setUp(self):

        self.password = 'password'

        self.admin = User.objects.create_superuser('admin', 'myemail@test.com', self.password)

        p = smodels.Position(title="title", slug="slug")
        p.save()
        sp = smodels.Position(title="super_title", slug="super_slug")
        sp.save()
        dpt =  smodels.Department(title="title",slug="slug")
        dpt.save()

        self.super = smodels.Staff(
            first_name="super",
            last_name="visor",
            position=sp,
            department=dpt,
            is_supervisor=False,
            profile=self.admin
        )
        self.super.save()

        self.staff = smodels.Staff(
            first_name="first",
            last_name="last",
            position=p,
            position_no="1234",
            reports_to=self.super,
        )
        self.staff.save()

    def gen_rounding(self):

        q = models.RoundingQuarter.objects.current_quarter()
        tools = "%s%s%s"%(faker.text(), models.Rounding.STOP_LIGHT_DELIM, faker.text())

        r = models.Rounding(
            staff=self.staff,
            roundingquarter=q,
            rounded_by=self.staff.reports_to.profile,
            rounded_on=timezone.now().date(),
            working=faker.text(),
            tools=tools,
            systems=faker.text(),
            quality=faker.text()
        )
        r.save()
        return r

    def test_valid(self):

        data = { "type":models.SL_TOOLS, "description":"test","status":models.SL_INPROGRESS,"comments":"test" }
        f = forms.StopLightItemForm(data=data)
        self.assertTrue(f.is_valid())

    def test_readonly_duplicate(self):
        r = self.gen_rounding()

        sli1 = models.StopLightItem(
            rounding = r,
            type = models.SL_TOOLS,
            description = "test",
            status= models.SL_COMPLETE,
            votes=2
        )
        sli1.save()

        sli2 = models.StopLightItem(
            rounding = r,
            type = models.SL_TOOLS,
            description = "test",
            status= models.SL_COMPLETE,
            votes=0,
            duplicate_of=sli1
        )
        sli2.save()

        f = forms.StopLightItemForm(instance=sli2)
        for field in f.fields:
            self.assertIn("readonly", f.fields[field].widget.attrs)

    def test_failed_closing(self):

        data = { "type":models.SL_TOOLS, "description":"test","status":models.SL_INVALID,}
        f = forms.StopLightItemForm(data)
        with self.assertRaises(ValidationError):
            f.is_valid()
            f.clean()

    def test_valid_closing(self):

        data = { "type":models.SL_TOOLS, "description":"test","status":models.SL_INVALID,"comments":"test"}
        f = forms.StopLightItemForm(data)
        self.assertTrue(f.is_valid())
