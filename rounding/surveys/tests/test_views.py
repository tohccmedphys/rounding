from django.test import TestCase, LiveServerTestCase
from .. import models
from staff import models as smodels

from django.conf import settings
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.http import Http404
from django.test.client import Client

from selenium.webdriver.firefox.webdriver import WebDriver
from selenium.webdriver.support.wait import WebDriverWait

import django.utils.timezone as timezone

from faker import Faker
import random

faker = Faker()

MAR = 3
JUN = 6
SEP = 9
DEC = 12

class BaseRoundingTestMixin(object):
    npositions = 4
    ndepartments = 4
    nstaff = 30
    ninactive = 4

    def setUp(self):

        self.password = "pwd"

        self.admin1 = User.objects.create_superuser('admin1', 'myemail1@test.com', self.password)
        self.admin2 = User.objects.create_superuser('admin2', 'myemail2@test.com', self.password)

        all_pos = []
        for i in range(self.npositions):
            p = smodels.Position(title="pos%d"%i, slug="slug%d"%i)
            p.save()
            all_pos.append(p)

        sp = smodels.Position(title="superpos", slug="slug%d"%i)
        sp.save()

        all_dpt = []
        for i in range(self.ndepartments):
            dpt =  smodels.Department(title="title%d"%i,slug="slug%d"%i)
            dpt.save()
            all_dpt.append(dpt)

        self.super1 = smodels.Staff(
            first_name="super1",
            last_name="visor",
            department=dpt,
            is_supervisor=True,
            position=sp,
            profile=self.admin1
        )
        self.super1.save()

        self.super2 = smodels.Staff(
            first_name="super2",
            last_name="visor",
            position=sp,
            department=dpt,
            is_supervisor=True,
            profile=self.admin2
        )
        self.super2.save()
        all_sup = [self.super1, self.super2]

        self.all_staff = []
        for x in range(self.nstaff):
            staff = smodels.Staff(
                first_name="first%d"%x,
                last_name="last",
                position=random.choice(all_pos),
                department=random.choice(all_dpt),
                position_no="%d"%x,
                reports_to=random.choice(all_sup),
            )
            staff.save()

        self.generate_rounding_set()

    def generate_rounding_set(self):
        q = models.RoundingQuarter.objects.current_quarter()

        nrecs = 0
        nthanks = 0
        nrounds = 0

        all_staff = list(smodels.Staff.objects.filter(is_supervisor=False))

        for i in range(self.ninactive):
            s = all_staff.pop(random.randint(0,len(all_staff)-1))
            s.toggle_status()
            s.save()


        total_staff = len(all_staff)

        for staff in smodels.Staff.objects.filter(is_supervisor=False):
            if models.InactiveQuarter.objects.filter(staff=staff).count()>0:
                continue

            if random.random() < 0.1:
                continue

            nrounds += 1

            r = models.Rounding(
                staff=staff,
                roundingquarter=q,
                rounded_by=staff.reports_to.profile,
                rounded_on=timezone.now().date(),
                working=faker.text(),
                tools=faker.text(),
                systems=faker.text(),
                quality=faker.text()
            )
            r.save()
            models.generate_stop_light_items_for_rounding(r)

            if random.random()<0.2:
                nrecs += 1
                recognized = random.choice(smodels.Staff.objects.all())

                rec = models.Recognition(
                    rounding=r,
                    recognized=recognized,
                    staff_type=models.Recognition.STAFF,
                    reason=faker.text(),
                )

                if random.choice([True, False]):
                    rec.thank_you_sent_by = random.choice(smodels.Staff.objects.filter(is_supervisor=True)).profile
                    rec.thank_you_sent = True
                    nthanks +=1
                rec.save()


class BaseRoundingTest(BaseRoundingTestMixin, TestCase):
    pass


class BaseRoundingLiveTest(BaseRoundingTestMixin, LiveServerTestCase):

    @classmethod
    def setUpClass(cls):
        cls.selenium = WebDriver()
        super(BaseRoundingLiveTest, cls).setUpClass()

    @classmethod
    def tearDownClass(cls):
        import time
        time.sleep(6)
        cls.selenium.quit()
        super(BaseRoundingLiveTest, cls).tearDownClass()

    def setUp(self):
        super(BaseRoundingLiveTest, self).setUp()

        self.q = models.RoundingQuarter.objects.current_quarter()

        url = '%s%s' % (self.live_server_url,"/admin/")
        self.selenium.get(url)
        username_input = self.selenium.find_element_by_name("username")
        username_input.send_keys(self.admin1.username)
        password_input = self.selenium.find_element_by_name("password")
        password_input.send_keys(self.password)
        self.selenium.find_element_by_xpath('//input[@value="Log in"]').click()

        WebDriverWait(self.selenium, 10).until(lambda driver: driver.find_element_by_tag_name('body'))


class TestRoundingEntry(BaseRoundingLiveTest):

    def fill_form(self):

        inp = self.selenium.find_element_by_id('id_working')
        inp.send_keys("working well")

        inp = self.selenium.find_element_by_class_name('tools')
        inp.send_keys("tools")

        inp = self.selenium.find_element_by_class_name('systems')
        inp.send_keys("systems")

        inp = self.selenium.find_element_by_class_name('quality')
        inp.send_keys("quality")

    def test_valid_with_rec(self):
        staff = smodels.Staff.objects.filter(is_supervisor=False)[0]
        url = self.live_server_url+reverse("round-staff", kwargs={"staff_pk":staff.pk})
        self.selenium.get(url)

        self.fill_form()

        self.selenium.find_element_by_id( "id_form-0-recognized").find_elements_by_tag_name( "option")[1].click()
        self.selenium.find_element_by_id( "id_form-0-staff_type").find_elements_by_tag_name( "option")[1].click()
        self.selenium.find_element_by_id( "id_form-0-reason").send_keys("good job")

        self.selenium.find_element_by_css_selector('button[type="submit"]').click()

        WebDriverWait(self.selenium, 10).until(lambda driver: driver.find_element_by_class_name('sorting_1'))
        self.assertIsNotNone(self.selenium.find_element_by_class_name("alert-success"))

    def test_valid_with_invalid_rec(self):
        staff = smodels.Staff.objects.filter(is_supervisor=False)[0]
        url = self.live_server_url+reverse("round-staff", kwargs={"staff_pk":staff.pk})
        self.selenium.get(url)

        self.fill_form()

        self.selenium.find_element_by_id( "id_form-0-recognized").find_elements_by_tag_name( "option")[1].click()
        self.selenium.find_element_by_id( "id_form-0-recognized_other").send_keys("other")
        self.selenium.find_element_by_id( "id_form-0-staff_type").find_elements_by_tag_name( "option")[1].click()
        self.selenium.find_element_by_id( "id_form-0-reason").send_keys("good job")

        self.selenium.find_element_by_css_selector('button[type="submit"]').click()

        WebDriverWait(self.selenium, 10).until(lambda driver: driver.find_element_by_tag_name('body'))
        self.assertIsNotNone(self.selenium.find_element_by_class_name("alert-error"))

    def test_valid_no_rec(self):

        staff = smodels.Staff.objects.filter(is_supervisor=False)[0]
        url = self.live_server_url+reverse("round-staff", kwargs={"staff_pk":staff.pk})
        self.selenium.get(url)

        self.fill_form()

        self.selenium.find_element_by_css_selector('button[type="submit"]').click()

        WebDriverWait(self.selenium, 10).until(lambda driver: driver.find_element_by_class_name('sorting_1'))
        self.assertIsNotNone(self.selenium.find_element_by_class_name("alert-success"))


class TestRoundingDetail(BaseRoundingLiveTest):


    def test_invalid_form(self):
        recog = models.Recognition.objects.get(pk=1)
        url = self.live_server_url+reverse("rounding-detail", kwargs={"pk":recog.rounding.pk})

        self.selenium.get(url)

        inp = self.selenium.find_element_by_id('id_recognition_set-0-recognized_other')
        inp.send_keys("foo")
        self.selenium.find_element_by_css_selector('button[type="submit"]').click()

        WebDriverWait(self.selenium, 10).until(lambda driver: driver.find_element_by_tag_name('body'))
        self.assertIsNotNone(self.selenium.find_element_by_class_name('error'))

    def test_set_thank_you(self):
        recog = models.Recognition.objects.get(pk=1)
        recog.thank_you_sent=False
        recog.thank_you_sent_by = None
        recog.save()
        url = self.live_server_url+reverse("rounding-detail", kwargs={"pk":recog.rounding.pk})

        self.selenium.get(url)

        self.selenium.find_element_by_id('id_recognition_set-0-thank_you_sent').click()
        self.selenium.find_element_by_css_selector('button[type="submit"]').click()

        WebDriverWait(self.selenium, 10).until(lambda driver: driver.find_element_by_tag_name('body'))
        recog = models.Recognition.objects.get(pk=1)
        self.assertTrue(recog.thank_you_sent)
        self.assertIsNotNone(recog.thank_you_sent_by)

    def test_set_no_thank_you(self):
        recog = models.Recognition.objects.get(pk=1)
        recog.thank_you_sent=True
        recog.thank_you_sent_by = self.admin1
        recog.save()
        url = self.live_server_url+reverse("rounding-detail", kwargs={"pk":recog.rounding.pk})

        self.selenium.get(url)

        self.selenium.find_element_by_id('id_recognition_set-0-thank_you_sent').click()
        self.selenium.find_element_by_css_selector('button[type="submit"]').click()

        WebDriverWait(self.selenium, 10).until(lambda driver: driver.find_element_by_tag_name('body'))
        recog = models.Recognition.objects.get(pk=1)
        self.assertFalse(recog.thank_you_sent)
        self.assertIsNone(recog.thank_you_sent_by)

class TestDataTablesViews(BaseRoundingLiveTest):


    def test_stop_light_items(self):
        url = self.live_server_url+reverse("stop-light-items")
        self.selenium.get(url)
        WebDriverWait(self.selenium, 10).until(lambda driver: driver.find_element_by_class_name('sorting_1'))

    def test_cur_rounding_quarter_detail(self):
        url = self.live_server_url+reverse("current-quarter")
        self.selenium.get(url)
        WebDriverWait(self.selenium, 10).until(lambda driver: driver.find_element_by_class_name('sorting_1'))


class TestRoundingQuarter(BaseRoundingTest):

    def setUp(self):
        super(TestRoundingQuarter, self).setUp()
        self.client.login(username=self.admin1.username, password=self.password)
        self.q = models.RoundingQuarter.objects.current_quarter()

    def test_rq_list(self):
        url = reverse("rounding-quarters")
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)


class TestDuplicate(BaseRoundingTest):

    def setUp(self):
        super(TestDuplicate, self).setUp()
        self.client.login(username=self.admin1.username, password=self.password)
        self.q = models.RoundingQuarter.objects.current_quarter()

    def mark_dup(self):
        url = reverse("stop-light-item-duplicate")
        self.client.post(url,data={"duplicates":"1,2"})

    def test_dup(self):
        self.mark_dup()

        sli1 = models.StopLightItem.objects.get(pk=1)
        sli2 = models.StopLightItem.objects.get(pk=2)

        self.assertEqual(sli1.votes,2)
        self.assertEqual(sli2.votes,0)
        self.assertEqual(sli2.duplicate_of,sli1)

    def test_dedup(self):
        self.mark_dup()
        url = reverse("dedup",kwargs={"pk":"2"})
        self.client.post(url)

        sli1 = models.StopLightItem.objects.get(pk=1)
        sli2 = models.StopLightItem.objects.get(pk=2)

        self.assertEqual(sli1.votes,1)
        self.assertEqual(sli2.votes,1)
        self.assertIsNone(sli2.duplicate_of)


class TestReports(BaseRoundingTest):

    def setUp(self):
        super(TestReports, self).setUp()
        self.client.login(username=self.admin1.username, password=self.password)
        self.q = models.RoundingQuarter.objects.current_quarter()

    def test_summary(self):
        url = reverse("rounding-summary")
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_gen_summary_no_quarter(self):
        url = reverse("generate-summary", kwargs={"pk":9999})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test__gen_summary(self):
        url = reverse("generate-summary", kwargs={"pk":self.q.pk})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_stars(self):
        url = reverse("recognition-stars")
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_gen_stars_no_quarter(self):
        url = reverse("generate-recognition-stars", kwargs={"pk":9999})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_gen_stars(self):
        url = reverse("generate-recognition-stars", kwargs={"pk":self.q.pk})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_recognition(self):
        url = reverse("recognition")
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_gen_recognition_no_quarter(self):
        url = reverse("generate-recognition", kwargs={"pk":9999})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_gen_recognition(self):
        url = reverse("generate-recognition", kwargs={"pk":self.q.pk})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_raw(self):
        url = reverse("raw-response")
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_gen_raw_no_quarter(self):
        url = reverse("generate-raw-response", kwargs={"pk":9999})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_gen_raw(self):
        url = reverse("generate-raw-response", kwargs={"pk":self.q.pk})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_stop_light(self):
        url = reverse("stop-light")
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_gen_stop_light_no_quarter(self):
        url = reverse("generate-stop-light", kwargs={"pk":9999})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_gen_stop_light(self):
        url = reverse("generate-stop-light", kwargs={"pk":self.q.pk})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_gen_stop_light_with_dpt(self):
        url = reverse("generate-stop-light", kwargs={"pk":self.q.pk})
        response = self.client.get(url+"?dept=1")
        self.assertEqual(response.status_code, 200)

    def test_gen_static_stop_light_no_quarter(self):
        url = reverse("static-stop-light", kwargs={"pk":9999})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_gen_static_stop_light(self):
        url = reverse("static-stop-light", kwargs={"pk":self.q.pk})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_gen_static_stop_light_quarter_with_dpt(self):
        url = reverse("static-stop-light", kwargs={"pk":self.q.pk})
        response = self.client.get(url+"?dept=1")
        self.assertEqual(response.status_code, 200)

    def test_gen_static_stop_light_bad_dpt(self):
        url = reverse("static-stop-light", kwargs={"pk":self.q.pk})
        response = self.client.get(url+"?dept=abc")
        self.assertEqual(response.status_code, 200)


#class TestRoundingView(TestCase):
#
#    def setUp(self):
#
#        self.password = 'password'
#
#        self.admin = User.objects.create_superuser('admin', 'myemail@test.com', self.password)
#
#        p = smodels.Position(title="title", slug="slug")
#        p.save()
#        sp = smodels.Position(title="super_title", slug="super_slug")
#        sp.save()
#        dpt =  smodels.Department(title="title",slug="slug")
#        dpt.save()
#
#        self.super = smodels.Staff(
#            first_name="super",
#            last_name="visor",
#            position=sp,
#            department=dpt,
#            is_supervisor=False,
#            profile=self.admin
#        )
#        self.super.save()
#
#        self.staff = smodels.Staff(
#            first_name="first",
#            last_name="last",
#            position=p,
#            position_no="1234",
#            reports_to=self.super,
#        )
#        self.staff.save()
#
#        self.url = reverse("rounding")
#
#        self.client.login(username=self.admin.username, password=self.password)
#
#    def test_denies_anon(self):
#        self.client.logout()
#        login = settings.LOGIN_URL+"?next=%s" % (self.url)
#        response = self.client.get(self.url,follow=True)
#        self.assertRedirects(response, login)
#
#    def test_logged_in(self):
#        response = self.client.get(self.url)
#        self.assertEqual(response.status_code, 200)
#
#    def test_basic_rounding(self):
#        q = models.RoundingQuarter.objects.current_quarter()
#
#        data = {
#            "roundingquarter":q.pk,
#            "staff":self.staff.pk,
#        }
#        response = self.client.post(self.url, data=data)
#        self.assertEqual(response.status_code, 302)
