from django.test import TestCase
from .. import models
from staff import models as smodels

from django.conf import settings
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.http import Http404
from django.test.client import Client

import django.utils.timezone as timezone
from faker import Faker
import random

faker = Faker()

MAR = 3
JUN = 6
SEP = 9
DEC = 12

class TestRoundingQuarter(TestCase):

    def setUp(self):
        self.password = "pwd"

        admin1 = User.objects.create_superuser('admin1', 'myemail1@test.com', self.password)
        admin2 = User.objects.create_superuser('admin2', 'myemail2@test.com', self.password)

        all_pos = []
        for i in range(4):
            p = smodels.Position(title="pos%d"%i, slug="slug%d"%i)
            p.save()
            all_pos.append(p)

        sp = smodels.Position(title="superpos", slug="slug%d"%i)
        sp.save()

        all_dpt = []
        for i in range(4):
            dpt =  smodels.Department(title="title%d"%i,slug="slug%d"%i)
            dpt.save()
            all_dpt.append(dpt)

        self.super1 = smodels.Staff(
            first_name="super1",
            last_name="visor",
            department=dpt,
            is_supervisor=True,
            position=sp,
            profile=admin1
        )
        self.super1.save()

        self.super2 = smodels.Staff(
            first_name="super2",
            last_name="visor",
            position=sp,
            department=dpt,
            is_supervisor=True,
            profile=admin2
        )
        self.super2.save()
        all_sup = [self.super1, self.super2]

        self.all_staff = []
        for x in range(30):
            staff = smodels.Staff(
                first_name="first%d"%x,
                last_name="last",
                position=random.choice(all_pos),
                department=random.choice(all_dpt),
                position_no="%d"%x,
                reports_to=random.choice(all_sup),
            )
            staff.save()

    def test_current(self):
        cur = models.RoundingQuarter.objects.current_quarter()

        t = timezone.datetime.today()

        if t.month <= MAR:
            year = t.year-1
            q = 4
        elif t.month <= JUN:
            year = t.year
            q = 1
        elif t.month <= SEP:
            year = t.year
            q = 2
        elif t.month <= DEC:
            year = t.year
            q = 3

        self.assertEqual((cur.quarter, cur.year), (q, year))

    def test_default_none(self):
        self.assertIsNone(models.RoundingQuarter.objects.default())

    def test_default_on_save(self):
        q = models.RoundingQuarter(quarter=1, year=2014)
        q.save()
        self.assertEqual(q,models.RoundingQuarter.objects.default())

    def test_name_on_save(self):
        q = models.RoundingQuarter(quarter=1, year=2014)
        self.assertEqual('',q.name)
        q.save()
        self.assertNotEqual('',q.name)

    def test_existing_default_on_save(self):
        q1 = models.RoundingQuarter(quarter=1, year=2014)
        q1.save()
        q2 = models.RoundingQuarter(quarter=2, year=2014)
        q2.save()
        self.assertNotEqual(q2,models.RoundingQuarter.objects.default())

    def gen_rounding(self):

        q = models.RoundingQuarter.objects.current_quarter()
        staff = random.choice(smodels.Staff.objects.filter(is_supervisor=False))
        tools = "%s%s%s"%(faker.text(), models.Rounding.STOP_LIGHT_DELIM, faker.text())

        r = models.Rounding(
            staff=staff,
            roundingquarter=q,
            rounded_by=staff.reports_to.profile,
            rounded_on=timezone.now().date(),
            working=faker.text(),
            tools=tools,
            systems=faker.text(),
            quality=faker.text()
        )
        r.save()
        return r

    def test_sli(self):
        r = self.gen_rounding()
        models.generate_stop_light_items_for_rounding(r)
        self.assertEqual(len(models.StopLightItem.objects.filter(status=models.SL_INPROGRESS)),4)

    def test_display_sli(self):
        r = self.gen_rounding()
        r.tools = "test%stestagain" % (models.Rounding.STOP_LIGHT_DELIM)
        models.generate_stop_light_items_for_rounding(r)
        self.assertEqual("<ul><li>test</li><li>testagain</li></ul>", r.display_sli("tools"))


    def test_summary(self):

        q = models.RoundingQuarter.objects.current_quarter()

        nrecs = 0
        nthanks = 0
        nrounds = 0

        ninactive = 4
        all_staff = list(smodels.Staff.objects.filter(is_supervisor=False))

        for i in range(ninactive):
            s = all_staff.pop(random.randint(0,len(all_staff)-1))
            s.toggle_status()
            s.save()


        total_staff = len(all_staff)

        for staff in smodels.Staff.objects.filter(is_supervisor=False):
            if models.InactiveQuarter.objects.filter(staff=staff).count()>0:
                continue

            if random.random() < 0.1:
                continue

            nrounds += 1

            r = models.Rounding(
                staff=staff,
                roundingquarter=q,
                rounded_by=staff.reports_to.profile,
                rounded_on=timezone.now().date(),
                working=faker.text(),
                tools=faker.text(),
                systems=faker.text(),
                quality=faker.text()
            )
            r.save()

            if random.random()<0.2:
                nrecs += 1
                recognized = random.choice(smodels.Staff.objects.all())

                rec = models.Recognition(
                    rounding=r,
                    recognized=recognized,
                    staff_type=models.Recognition.STAFF,
                    reason=faker.text(),
                )

                if random.choice([True, False]):
                    rec.thank_you_sent_by = random.choice(smodels.Staff.objects.filter(is_supervisor=True)).profile
                    rec.thank_you_sent = True
                    nthanks +=1
                rec.save()


        self.assertListEqual(["Total", nrounds, total_staff, nrecs, nthanks], list(q.summary()[-1]))


    def test_status_display(self):
        r = self.gen_rounding()
        models.generate_stop_light_items_for_rounding(r)
        sli = models.StopLightItem.objects.filter(type=models.SL_TOOLS)[0]
        self.assertEqual(sli.status_display(),"In Progress")

    def test_type_display(self):
        r = self.gen_rounding()
        models.generate_stop_light_items_for_rounding(r)
        sli = models.StopLightItem.objects.filter(type=models.SL_TOOLS)[0]
        self.assertEqual(sli.type_display(),models.STOP_LIGHT_TYPE_DISPLAY[sli.type])


    def test_complete(self):
        cur = models.RoundingQuarter.objects.current_quarter()
        r = self.gen_rounding()
        models.generate_stop_light_items_for_rounding(r)
        sli = models.StopLightItem.objects.filter(type=models.SL_TOOLS)[0]
        sli.status = models.SL_COMPLETE
        sli.save()
        self.assertEqual(sli.quarter_completed,cur)
