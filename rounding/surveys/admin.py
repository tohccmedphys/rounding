from django.contrib import admin

from surveys import models
from staff.models import Staff

def reports_to(obj):
    if obj.recognized and obj.recognized.reports_to:
        return obj.recognized.reports_to.name()
    return ""

def staff_name(obj):
    return obj.staff.name()


def rounded_by(obj):
    return obj.rounded_by.username

class RoundingAdmin(admin.ModelAdmin):

    list_display = ("id", staff_name, rounded_by, "rounded_on", "roundingquarter")


    def queryset(self, request):
        qs = super(RoundingAdmin, self).queryset(request)
        return qs.select_related("staff","rounded_by","roundingquarter")



class SupervisorListFilter(admin.SimpleListFilter):
    title = 'Supervisor'

    parameter_name = 'supervisor'

    def lookups(self, request, model_admin):
        q = Staff.objects.filter(is_supervisor=True)
        return [(x.pk, x.name()) for x in q]

    def queryset(self, request, queryset):
        v = self.value()
        if v is not None:
            return queryset.filter(recognized__reports_to__pk=self.value())
        return queryset.select_related("rounding__roundingquarter","recognized__reports_to")


class RecognitionAdmin(admin.ModelAdmin):
    list_editable = ("thank_you_sent","thank_you_sent_by",)
    list_filter = ("thank_you_sent", SupervisorListFilter)
    list_display = ("recognized", "recognized_other","rounding", reports_to, "staff_type", "thank_you_sent", "thank_you_sent_by",)


class InactiveAdmin(admin.ModelAdmin):
    list_display = ("staff", "quarter",)

class RoundingQuarterAdmin(admin.ModelAdmin):
    list_display = ("name", "default",)

def sli_quarter(obj):
    return obj.rounding.roundingquarter

def suggested_by(obj):
    return obj.rounding.staff

def rounded_by_sli(obj):
    return obj.rounding.rounded_by

class StopLightItemAdmin(admin.ModelAdmin):
    list_display = ("id",sli_quarter,rounded_by_sli, suggested_by, "type", "status")
    list_filter = ("type", "status",)
    raw_id_fields = ("rounding", "quarter_completed",)
    search_fields = ["rounding__roundingquarter__year","description"]

    def queryset(self, request):
        qs = super(StopLightItemAdmin, self).queryset(request)
        return qs.select_related("rounding__staff","rounding__rounded_by","rounding__roundingquarter")

admin.site.register([models.Rounding], RoundingAdmin)
admin.site.register([models.Recognition],RecognitionAdmin)
admin.site.register([models.RoundingQuarter],RoundingQuarterAdmin)
admin.site.register([ models.StopLightItem ],StopLightItemAdmin)
admin.site.register([ models.InactiveQuarter],InactiveAdmin)
