from django.conf import settings
from django.utils.translation import ugettext as _
from django.contrib.auth import authenticate
from django.forms import ModelForm, Textarea, ValidationError, ModelChoiceField
from django.forms.models import modelformset_factory, inlineformset_factory
import models


class ShortTextMixin(object):

    def __init__(self, *args, **kwargs):

        super(ShortTextMixin, self).__init__(*args, **kwargs)

        for f in self.fields.values():
            if isinstance(f.widget, Textarea):
                f.widget.attrs['rows'] = 4

class RecognitionForm(ShortTextMixin, ModelForm):
    class Meta:
        model = models.Recognition
        fields=("recognized", "recognized_other", "staff_type", "reason",)

    def clean(self):

        cleaned_data = super(RecognitionForm, self).clean()

        if bool(cleaned_data.get("recognized_other")) == bool(cleaned_data.get("recognized")):
            raise ValidationError("Enter either 'Recognized Other' or 'Recognized'")
        return cleaned_data


class RecognitionUpdateForm(RecognitionForm):
    class Meta:
        model = models.Recognition
        fields=("recognized", "recognized_other", "staff_type", "reason","thank_you_sent","thank_you_sent_by",)


BaseRecognitionFormset = modelformset_factory(models.Recognition, form=RecognitionForm)

class RecognitionFormset(BaseRecognitionFormset):
    pass


class RoundingForm(ShortTextMixin, ModelForm):

    roundingquarter = ModelChoiceField(models.RoundingQuarter.objects, initial=lambda:models.RoundingQuarter.objects.default(), label=_("Rounding Quarter"))

    class Meta:
        model = models.Rounding

    def __init__(self, *args, **kwargs):
        super(RoundingForm, self).__init__(*args, **kwargs)

        if 'staff' in self.initial:
            for f in ["roundingquarter", "staff"]:
                self.fields[f].widget.attrs['readonly'] = "readonly"


class RoundingUpdateForm(ShortTextMixin, ModelForm):
    class Meta:

        model = models.Rounding

        fields = ["working", "tools", "systems", "quality"]


UpdateRecognitionFormset = inlineformset_factory(
    models.Rounding, models.Recognition,
    form=RecognitionUpdateForm,
    extra=1,
    can_delete=False,
)


class StopLightItemForm(ModelForm):

    class Meta:
        model = models.StopLightItem
        fields = ["type", "description", "status", "comments"]

    def __init__(self, *args, **kwargs):

        super(StopLightItemForm, self).__init__(*args, **kwargs)

        if self.instance and self.instance.duplicate_of:
            for f in self.fields.values():
                f.widget.attrs['readonly'] = "readonly"

    def clean(self):
        cleaned_data = super(StopLightItemForm, self).clean()

        if cleaned_data.get("status") == models.SL_INVALID  and cleaned_data.get("comments") == "":
            raise ValidationError("Please enter a comment about why this item can not be addressed")
        return cleaned_data

