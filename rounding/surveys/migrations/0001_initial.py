# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'RoundingQuarter'
        db.create_table(u'surveys_roundingquarter', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('year', self.gf('django.db.models.fields.PositiveIntegerField')(default=2014)),
            ('quarter', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('default', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=20)),
        ))
        db.send_create_signal(u'surveys', ['RoundingQuarter'])

        # Adding unique constraint on 'RoundingQuarter', fields ['year', 'quarter']
        db.create_unique(u'surveys_roundingquarter', ['year', 'quarter'])

        # Adding model 'Rounding'
        db.create_table(u'surveys_rounding', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('roundingquarter', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['surveys.RoundingQuarter'])),
            ('staff', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['staff.Staff'])),
            ('rounded_by', self.gf('django.db.models.fields.related.ForeignKey')(related_name='rounding_rounded_by', to=orm['auth.User'])),
            ('rounded_on', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('working', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('tools', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('systems', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('quality', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'surveys', ['Rounding'])

        # Adding model 'Recognition'
        db.create_table(u'surveys_recognition', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('rounding', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['surveys.Rounding'])),
            ('recognized', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['staff.Staff'], null=True, blank=True)),
            ('recognized_other', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('staff_type', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('reason', self.gf('django.db.models.fields.TextField')()),
            ('thank_you_sent', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('thank_you_sent_by', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='recognition_thank_you_sent_by_user', null=True, to=orm['auth.User'])),
        ))
        db.send_create_signal(u'surveys', ['Recognition'])

        # Adding model 'StopLightItem'
        db.create_table(u'surveys_stoplightitem', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('rounding', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['surveys.Rounding'])),
            ('type', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('description', self.gf('django.db.models.fields.TextField')()),
            ('comments', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('status', self.gf('django.db.models.fields.CharField')(max_length=15)),
            ('quarter_completed', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['surveys.RoundingQuarter'], null=True, blank=True)),
            ('votes', self.gf('django.db.models.fields.PositiveIntegerField')(default=1)),
            ('duplicate_of', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['surveys.StopLightItem'], null=True, blank=True)),
        ))
        db.send_create_signal(u'surveys', ['StopLightItem'])

        # Adding model 'InactiveQuarter'
        db.create_table(u'surveys_inactivequarter', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('staff', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['staff.Staff'])),
            ('quarter', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['surveys.RoundingQuarter'])),
        ))
        db.send_create_signal(u'surveys', ['InactiveQuarter'])


    def backwards(self, orm):
        # Removing unique constraint on 'RoundingQuarter', fields ['year', 'quarter']
        db.delete_unique(u'surveys_roundingquarter', ['year', 'quarter'])

        # Deleting model 'RoundingQuarter'
        db.delete_table(u'surveys_roundingquarter')

        # Deleting model 'Rounding'
        db.delete_table(u'surveys_rounding')

        # Deleting model 'Recognition'
        db.delete_table(u'surveys_recognition')

        # Deleting model 'StopLightItem'
        db.delete_table(u'surveys_stoplightitem')

        # Deleting model 'InactiveQuarter'
        db.delete_table(u'surveys_inactivequarter')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'staff.department': {
            'Meta': {'object_name': 'Department'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '255'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'staff.position': {
            'Meta': {'object_name': 'Position'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '255'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'staff.staff': {
            'Meta': {'ordering': "('last_name', 'first_name')", 'object_name': 'Staff'},
            'department': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['staff.Department']", 'null': 'True', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_supervisor': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'position': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['staff.Position']"}),
            'position_no': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'profile': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']", 'null': 'True', 'blank': 'True'}),
            'reports_to': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['staff.Staff']", 'null': 'True', 'blank': 'True'})
        },
        u'surveys.inactivequarter': {
            'Meta': {'object_name': 'InactiveQuarter'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'quarter': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['surveys.RoundingQuarter']"}),
            'staff': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['staff.Staff']"})
        },
        u'surveys.recognition': {
            'Meta': {'object_name': 'Recognition'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'reason': ('django.db.models.fields.TextField', [], {}),
            'recognized': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['staff.Staff']", 'null': 'True', 'blank': 'True'}),
            'recognized_other': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'rounding': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['surveys.Rounding']"}),
            'staff_type': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'thank_you_sent': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'thank_you_sent_by': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'recognition_thank_you_sent_by_user'", 'null': 'True', 'to': u"orm['auth.User']"})
        },
        u'surveys.rounding': {
            'Meta': {'object_name': 'Rounding'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'quality': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'rounded_by': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'rounding_rounded_by'", 'to': u"orm['auth.User']"}),
            'rounded_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'roundingquarter': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['surveys.RoundingQuarter']"}),
            'staff': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['staff.Staff']"}),
            'systems': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'tools': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'working': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'})
        },
        u'surveys.roundingquarter': {
            'Meta': {'ordering': "('year', 'quarter')", 'unique_together': "(('year', 'quarter'),)", 'object_name': 'RoundingQuarter'},
            'default': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'quarter': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'year': ('django.db.models.fields.PositiveIntegerField', [], {'default': '2014'})
        },
        u'surveys.stoplightitem': {
            'Meta': {'object_name': 'StopLightItem'},
            'comments': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'duplicate_of': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['surveys.StopLightItem']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'quarter_completed': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['surveys.RoundingQuarter']", 'null': 'True', 'blank': 'True'}),
            'rounding': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['surveys.Rounding']"}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'votes': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1'})
        }
    }

    complete_apps = ['surveys']