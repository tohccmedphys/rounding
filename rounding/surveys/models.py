from django.db import models
from django.db.models import Q
from django.core.urlresolvers import reverse, reverse_lazy
from django.conf import settings
from django.contrib.auth import get_user_model
from django.utils.translation import ugettext as _
from django.utils.formats import localize
import django.utils.timezone as timezone



User = get_user_model()

MARCH = MAR = 3
Q1 = [4, 5, 6]
Q2 = [7, 8, 9]
Q3 = [10, 11, 12]
Q4 = [1, 2, 3]

QUARTER_CHOICES = (
    (1, "Q1(Apr-Jun)"),
    (2, "Q2(Jul-Sep)"),
    (3, "Q3(Oct-Dec)"),
    (4, "Q4(Jan-Mar)"),
)
ALL_QS = (
    (1, Q1),
    (2, Q2),
    (3, Q3),
    (4, Q4),
)

SL_COMPLETE = "complete"
SL_INPROGRESS = "inprogress"
SL_INVALID = "invalid"

STOP_LIGHT_STATUS_CHOICES = (
    (SL_COMPLETE, "Complete"),
    (SL_INPROGRESS, "In Progress"),
    (SL_INVALID, "Can't Address"),
)

SL_TOOLS = "tools"
SL_SYSTEMS = "systems"
SL_SAFETY = "quality"

STOP_LIGHT_TYPES = (
    (SL_TOOLS, "Tools"),
    (SL_SYSTEMS, "Equipment & Systems"),
    (SL_SAFETY, "Quality & Safety"),
)

STOP_LIGHT_TYPE_DISPLAY = dict(STOP_LIGHT_TYPES)
STOP_LIGHT_STATUS_DISPLAY = dict(STOP_LIGHT_STATUS_CHOICES)

class RoundingManager(models.Manager):

    #----------------------------------------------------------------------
    def default(self):
        """return the default State"""
        try:
            defaults = list(self.get_query_set().filter(default=True))
            return defaults[-1]
        except IndexError:
            return

    def current_quarter(self):
        t = timezone.datetime.today()
        year = t.year - 1 if t.month <= MAR else t.year
        for q, months in ALL_QS:
            if  t.month in months:
                cur, _ = self.get_query_set().get_or_create(year=year, quarter=q)
                return cur


class RoundingQuarter(models.Model):

    year = models.PositiveIntegerField(default=timezone.now().year)
    quarter = models.PositiveIntegerField(choices=QUARTER_CHOICES, help_text=_("Choose the quarter this rounding is for"))
    default = models.BooleanField(default=False,  help_text=_("Make this the new default rounding quarter."))
    name = models.CharField(max_length=20)
    objects = RoundingManager()

    class Meta:
        unique_together = (("year", "quarter"),)
        ordering = ("year", "quarter",)

    def save(self, *args, **kwargs):
        self.set_name()

        if RoundingQuarter.objects.default() is None:
            self.default = True

        if self.default:
            RoundingQuarter.objects.filter(default=True).update(default=False)

        super(RoundingQuarter, self).save(*args, **kwargs)

    def summary(self):

        from staff.models import Staff

        supervisors = Staff.objects.filter(is_supervisor=True).exclude(profile=None).order_by("last_name")
        rows = []

        total_rounded, total_staff, total_recognitions, total_thanks = 0, 0, 0, 0

        for supervisor in supervisors:
            roundings = self.rounding_set.filter(rounded_by=supervisor.profile)
            num_rounded = roundings.count()
            num_staff = Staff.objects.filter(
                reports_to=supervisor,
            ).exclude(
                inactivequarter__quarter=self
            ).count()

            recognitions = Recognition.objects.filter(
                rounding__roundingquarter=self,
            )
            thankyous = recognitions.filter(thank_you_sent=True,thank_you_sent_by=supervisor.profile ).count()
            num_recognitions = recognitions.filter(
                rounding__rounded_by=supervisor.profile
            ).count()

            rows.append((supervisor, num_rounded, num_staff, num_recognitions, thankyous))

            total_rounded += num_rounded
            total_staff += num_staff
            total_recognitions += num_recognitions
            total_thanks += thankyous

        rows.append(("Total", total_rounded, total_staff, total_recognitions, total_thanks))
        return rows

    def set_name(self):
        q = QUARTER_CHOICES[[x[0] for x in QUARTER_CHOICES].index(self.quarter)][1]
        self.name = "%d-%s" % (self.year, q)
        return self.name

    def __unicode__(self):
        return self.name

def generate_stop_light_items_for_rounding(rounding):

    for attr in Rounding.STOP_LIGHT_ATTRS:
        f = getattr(rounding , attr)
        if f is not None and f.strip() != "":
            for d in f.split(Rounding.STOP_LIGHT_DELIM):
                sli = StopLightItem(
                    rounding = rounding,
                    description = d,
                    status = SL_INPROGRESS,
                    type=attr,
                )
                sli.save()

class Rounding(models.Model):

    STOP_LIGHT_ATTRS = ("tools", "systems", "quality",)
    STOP_LIGHT_DELIM = "&STOP_LIGHT_ITEM&"


    roundingquarter = models.ForeignKey(RoundingQuarter, default=lambda:RoundingQuarter.objects.default())

    staff = models.ForeignKey('staff.Staff', help_text=_("Choose the name of the staff being rounded"))

    rounded_by = models.ForeignKey(User, help_text=_("Choose the name of the staff doing rounding"), related_name="rounding_rounded_by", editable=False)
    rounded_on = models.DateTimeField(auto_now_add=True)

    working = models.TextField(verbose_name=_("Working well?"),help_text=_('What is working well for you today?'), null=True, blank=True)
    tools = models.TextField(verbose_name=_("Tools & Equipment"),help_text=_('Tools and equipment needed today?'), null=True, blank=True)
    systems = models.TextField(help_text=_('Systems you want to improve and your ideas to fix them?'),null=True, blank=True)
    quality = models.TextField(verbose_name=_("Quality & Safety"), help_text=_('Quality & Safety issues discussed.'), null=True, blank=True)

    def display_sli(self, attr):
        items = ["<li>%s</li>"%(txt) for txt in  getattr(self,attr,"").split(self.STOP_LIGHT_DELIM) if txt.strip()]
        display = "<ul>%s</ul>" % ("".join(items)) if items else ""
        return display

    def get_absolute_url(self):
        return reverse("rounding-detail",kwargs={"pk":self.pk})


    def __unicode__(self):
        return "%s" % (self.roundingquarter,)


class Recognition(models.Model):
    PHYSICIAN = "physician"
    STAFF = "staff"
    OTHER = "other"
    RECOGNITION_STAFF_TYPE_CHOICES = ((STAFF, _("Staff")), (PHYSICIAN, _("Physician")), (OTHER,_("Other")),)

    rounding = models.ForeignKey(Rounding)
    recognized = models.ForeignKey('staff.Staff', help_text=_("Staff being recognized (or use Recognized other field)"), null=True, blank=True)
    recognized_other = models.CharField(max_length=255,help_text=_("Use only for non regular staff"), null=True, blank=True)
    staff_type = models.CharField(max_length=255, choices=RECOGNITION_STAFF_TYPE_CHOICES)
    reason = models.TextField()
    thank_you_sent = models.BooleanField(default=False, help_text=_("Check if a thank you has been given"))
    thank_you_sent_by = models.ForeignKey(User, help_text=_("Thank you note sent by"), null=True, blank=True, related_name="recognition_thank_you_sent_by_user")


class StopLightItem(models.Model):

    rounding = models.ForeignKey(Rounding)
    type = models.CharField(max_length=10, choices=STOP_LIGHT_TYPES)
    description = models.TextField()
    comments = models.TextField(null=True, blank=True)
    status = models.CharField(max_length=15, choices=STOP_LIGHT_STATUS_CHOICES)
    quarter_completed = models.ForeignKey(RoundingQuarter, null=True, blank=True)

    votes = models.PositiveIntegerField(default=1)
    duplicate_of = models.ForeignKey('self', null=True, blank=True)

    def status_display(self):
        return STOP_LIGHT_STATUS_DISPLAY[self.status]

    def type_display(self):
        return STOP_LIGHT_TYPE_DISPLAY[self.type]

    def save(self):
        if self.status in (SL_INVALID, SL_COMPLETE) and self.quarter_completed == None:
            self.quarter_completed = RoundingQuarter.objects.current_quarter()

        super(StopLightItem, self).save()


class InactiveQuarter(models.Model):

    staff = models.ForeignKey('staff.Staff')
    quarter = models.ForeignKey(RoundingQuarter)

