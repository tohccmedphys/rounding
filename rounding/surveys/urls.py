from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView, RedirectView
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse_lazy
import views

urlpatterns = patterns('',

    url('^$', RedirectView.as_view(url=reverse_lazy('current-quarter'))),
    url("^round/$",views.Rounding.as_view(),name="rounding"),
    url("^round/staff/(?P<staff_pk>\d+)/$",views.Rounding.as_view(),name="round-staff"),
    url("^rounded/(?P<pk>\d+)/$",views.RoundingDetail.as_view(),name="rounding-detail"),
    url("^quarter/$",views.RoundingQuarterList.as_view(),name="rounding-quarters"),
    url("^quarter/current/$",views.RoundingQuarterDetail.as_view(),name="current-quarter"),
    url("^quarter/(?P<pk>\d+)/$",views.RoundingQuarterDetail.as_view(),name="rounding-quarter"),
    url("^quarter/(?P<pk>\d+)/data/$",views.RoundingQuarterDetailData.as_view(),name="rounding-quarter-dt"),
    url("^quarter/raw/$",views.RawResponseReport.as_view(),name="raw-response"),
    url("^quarter/raw/(?P<pk>\d+)/$",views.GenerateRawResponseReport.as_view(),name="generate-raw-response"),

    url("^stoplight/$",views.StopLightItems.as_view(),name="stop-light-items"),
    url("^stoplight/data/$",views.StopLightItemsData.as_view(),name="stop-light-items-dt"),
    url("^stoplight/(?P<pk>\d+)/$",views.StopLightItem.as_view(),name="stop-light-item"),
    url("^stoplight/duplicates/$",views.MarkDuplicate.as_view(),name="stop-light-item-duplicate"),
    url("^stoplight/duplicates/dedup/(?P<pk>\d+)/$",views.DeDuplicate.as_view(),name="dedup"),

    url("^report/summary/$",views.RoundingSummary.as_view(),name="rounding-summary"),
    url("^report/summary/(?P<pk>\d+)/$",views.GenerateSummary.as_view(),name="generate-summary"),

    url("^report/recognition/$",views.RecognitionReport.as_view(),name="recognition"),
    url("^report/recognition/(?P<pk>\d+)/$",views.GenerateRecognitions.as_view(),name="generate-recognition"),
    url("^report/recognition/stars/$",views.RecognitionStars.as_view(),name="recognition-stars"),
    url("^report/recognition/stars/(?P<pk>\d+)/$",views.GenerateRecognitionStars.as_view(),name="generate-recognition-stars"),

    url("^report/stoplight/$",views.StopLightReport.as_view(),name="stop-light"),
    url("^report/stoplight/(?P<pk>\d+)/$",views.GenerateStopLight.as_view(),name="generate-stop-light"),
    url("^report/stoplight/(?P<pk>\d+)/static/$",views.StaticStopLightReport.as_view(),name="static-stop-light"),
)

