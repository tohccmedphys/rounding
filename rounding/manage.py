#!/usr/bin/env python
import os
import sys

if __name__ == "__main__":
    s = "test" if "test" == sys.argv[1] else "local"
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "rounding.settings.%s"%s)

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
