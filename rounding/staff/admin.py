from django.contrib import admin
from django.forms import ModelForm, ValidationError

from staff import models

def supervisor_name(obj):
    if obj.reports_to:
        return obj.reports_to.name()
    return ""

class StaffAdminForm(ModelForm):
    def clean(self):
        cleaned_data = super(StaffAdminForm, self).clean()
        if cleaned_data['is_supervisor'] and not cleaned_data['profile']:
            self.errors['profile'] = self.error_class(["Supervisors must have a linked login profile!"])
        return cleaned_data

class StaffAdmin(admin.ModelAdmin):
    form = StaffAdminForm

    list_display = ("id", "position_no", "first_name", "last_name", supervisor_name, "department","is_supervisor")
    list_filter = ("department","is_supervisor",)

    def queryset(self, request):
        qs = super(StaffAdmin, self).queryset(request)
        return qs.select_related("department","reports_to")



admin.site.register([models.Staff], StaffAdmin)
admin.site.register([models.Department, models.Position],admin.ModelAdmin)
