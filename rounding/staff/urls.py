from django.conf.urls import patterns, url
import views

urlpatterns = patterns('',
    url('^toggle/(?P<pk>\d+)/$', views.ToggleStatus.as_view(),name="toggle-active"),
)
