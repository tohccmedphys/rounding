from django.db import models
from django.contrib.auth import get_user_model
from django.utils.translation import ugettext as _
from django.utils.formats import localize

User = get_user_model()

FYQTR_ACTIVE = 'active'
FYQTR_INACTIVE = 'inactive'

FYQTR_ACTIVE_CHOICES = (
    (FYQTR_ACTIVE,"Active"),
    (FYQTR_INACTIVE,"Inactive"),
)
FYQTR_ACTIVE_CHOICES_DISPLAY = dict(FYQTR_ACTIVE_CHOICES)

class Department(models.Model):
    title = models.CharField(max_length=255)
    slug = models.SlugField(help_text=_("URL Friendly version of title (e.g. radiation-oncology)"),max_length=255, unique=True)

    def __unicode__(self):
        return self.title

class Position(models.Model):
    title = models.CharField(max_length=255)
    slug = models.SlugField(help_text=_("URL Friendly version of title (e.g. radiation-therapist)"),max_length=255)

    def __unicode__(self):
        return self.title

class Staff(models.Model):

    first_name = models.CharField(max_length=255, help_text=_("Enter the name of the staff being rounded"))
    last_name = models.CharField(max_length=255, help_text=_("Enter the name of the staff being rounded"))
    position = models.ForeignKey(Position)
    position_no = models.CharField(max_length=255, null=True, blank=True)
    reports_to = models.ForeignKey('self', null=True, blank=True)
    department = models.ForeignKey(Department, null=True, blank=True)
    is_supervisor = models.BooleanField(default=False)
    profile = models.ForeignKey(User, null=True, blank=True)

    class Meta:
        verbose_name_plural = "staff"
        ordering = ("last_name", "first_name",)

    def name(self):
        return "%s, %s" % (self.last_name, self.first_name)

    def name_alt(self):
        return "%s %s" % (self.first_name, self.last_name, )

    def toggle_status(self, quarter=None):

        from surveys.models import InactiveQuarter, RoundingQuarter

        if not quarter:
            quarter = RoundingQuarter.objects.current_quarter()

        try:
            iq = InactiveQuarter.objects.get(staff=self, quarter=quarter)
            iq.delete()
        except InactiveQuarter.DoesNotExist:
            InactiveQuarter.objects.create(staff=self, quarter=quarter)

    def status(self, quarter=None, is_inactive=None):

        from surveys.models import InactiveQuarter, RoundingQuarter

        if quarter is None:
            quarter = RoundingQuarter.objects.current_quarter()

        if is_inactive is None:
            try:
                iq = InactiveQuarter.objects.get(staff=self, quarter=quarter)
                status = FYQTR_INACTIVE
            except InactiveQuarter.DoesNotExist:
                status = FYQTR_ACTIVE
        elif is_inactive == False:
            status=FYQTR_ACTIVE
        else:
            status=FYQTR_INACTIVE

        return FYQTR_ACTIVE_CHOICES_DISPLAY[status]

    def supervisor(self):
        return self.reports_to.name() if self.reports_to else ""

    def __unicode__(self):
        return self.name()


