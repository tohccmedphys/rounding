# Create your views here.
from braces.views import JSONResponseMixin, LoginRequiredMixin

from django.views.generic import View
from django.http import Http404

from . import models
from surveys.models import RoundingQuarter

class ToggleStatus(LoginRequiredMixin, JSONResponseMixin, View):
    raise_exception = True

    def get(self, *args, **kwargs):

        try:
            staff = models.Staff.objects.get(pk=self.kwargs["pk"])
        except models.Staff.DoesNotExist:
            raise Http404

        try:
            quarter = RoundingQuarter.objects.get(pk=self.request.GET.get("q"))
        except RoundingQuarter.DoesNotExist:
            quarter = None

        staff.toggle_status(quarter=quarter)
        staff.save()

        return self.render_json_response({"status":staff.status(quarter=quarter)})


