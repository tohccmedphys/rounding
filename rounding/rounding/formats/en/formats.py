DATETIME_FORMAT = "j M Y g:i A"
DATE_FORMAT = "j M Y"
DATE_INPUT_FORMATS = [
    "%d %b %Y",
    "%d %B %Y",
    "%d-%b-%Y",
    "%d-%B-%Y",
]
SHORT_DATE_FORMAT = DATE_FORMAT
