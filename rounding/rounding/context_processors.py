from django.conf import settings

def site(request):

    return {
        'VERSION': settings.VERSION,
        'BUG_REPORT_URL': settings.BUG_REPORT_URL,
    }
