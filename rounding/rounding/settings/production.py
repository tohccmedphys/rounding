"""Production settings and globals."""

from os import environ

from base import *

#database_settings contains database user/password information that should not go in version control!
from database_settings import *

#active_directory_settings contains database user/password information that should not go in version control!
from active_directory_settings import *
AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'accounts.backends.ActiveDirectoryGroupMembershipSSLBackend',
)

#email_settings contains database user/password information that should not go in version control!
from email_settings import *


DEFAULT_GROUP_NAMES = []
FORCE_SCRIPT_NAME = "/r/therapy"
LOGIN_REDIRECT_URL = "/r/therapy/quarter/"
LOGIN_URL = "/r/therapy/login"
STATIC_URL = '/rnd_static/'
ALLOWED_HOSTS = ["localhost"]



ADMINS = (
    ('Randle Taylor', 'rataylor@toh.on.ca'),
    ('Randle Taylor', 'randle.taylor@gmail.com'),
)
MANAGERS = ADMINS


