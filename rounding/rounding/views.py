from django.http import *
from django.shortcuts import render_to_response,redirect
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.conf import settings

def login_user(request):
    logout(request)
    username = password = ''
    if request.POST:
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None and user.is_active:
            login(request, user)
        next_ = request.POST.get("next") or settings.LOGIN_REDIRECT_URL
        return HttpResponseRedirect(next_)
    return render_to_response('login.html', context_instance=RequestContext(request))
